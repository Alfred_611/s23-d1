// CRUD Operations
/*
	Create - Insert
	Read - Find
	Update
	Delete
*/

// Inserting Documents (Create) - specific records sa collection/database

// Syntax: db.collectionName.insertOne({object});
// JavaScript: object.object.method({object}); - (just in case maecounter ang syntax na ito)
db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
                "phone": "09196543210",
                "email": "janedoe@mail.com"
            },
        "courses": ["CSS", "Javascript", "Python"],
        "department": "none"
    });

// Insert Many
// Syntax: db.collectionName.insertMany([{objectA}, {objectB}]); - pag multiple, lagyan ng array []
db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
                "phone": "0917987653",
                "email": "stephenhawking@mail.com"
            },
        "courses": ["React", "PHP", "Python"],
        "department": "none"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
                "phone": "09061234587",
                "email": "neilarmstrong@mail.com"
            },
        "courses": ["React", "Laravel", "SASS"],
        "department": "none"
    }
    ]);
// Finding documents (Read) Operation
/* Find Syntax:
	db.collectionName.find();
	db.collectionName.find({field: value});
*/

db.users.find

db.users.find({"lastName": "Doe"});

db.users.find({"lastName": "Doe","age": 25}).pretty(); // advance query na rin to eh talaga

db.users.find(ObjectId("61760aecdd1b8c162017d5aa"));

// Updating Documents (Update) Operation
// db.collectionName.updateOne({criteria}, {$set: {field: value})});

// Insert test document
db.users.updateOne({
        "firstName": "Test",
        "lastName": "Test",
        "age": 0,
        "contact": {
                "phone": "00000000000",
                "email": "test@mail.com"
            },
        "courses": [],
        "department": "none"
    });

// Update one document
db.users.updateOne(
			{"firstName": "test"},
        {
            $set: {
            "firstName": "Bill",
            "lastName": "Gates",
            "age": 65,
            "contact": {
                    "phone": "09170123465",
                    "email": "bill@mail.com"
                },
            "courses": ["PHP", "Laravel", "HTML"],
            "department": "none"
            }
        }
);

// Update multiple documents
db.users.updateMany(
		{"department": "none"},
		{
			$set: {"department": "HR"}
		}
	);

// Deleting Documents (Delete) Operation
// Syntax: db.collectionName.deleteOne({criteria}); - deleting a single document

//Create a single document with one field
db.users.insert({
    "firstName": "test"
});

// Delete one document
db.users.deleteOne({
    "firstName": "test" // kahit object id gumagana hanapin mo lang
});

// Update multiple documents where lastName is "Doe";
db.users.UpdateMany(
	{ "lastName": "Doe" },
	{
		$set: {
			"department": "Operations"
		}
	}
);

// Delete multiple documents
db.users.deleteMany(
		{"department": "Operations"}
	);

// Advance Query

// Query and embedded document - being more specific on documents we're looking for
db.users.find({
"contact": {
	"phone": "09170123465",
	"email": "bill@mail"
}
});

// Querying an Array without a specific order of elements
db.users.find({"courses": {$all: ["React", "Python"]} }); // in order dapat nasa array